<?php

/**
 * Private video module utility functions
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

// associated activities type
//define('VIDEO_ACTIVITY_QUIZ', 0);
//define('VIDEO_ACTIVITY_QUIZ', 0);

require_once("$CFG->libdir/filelib.php");
require_once("$CFG->libdir/resourcelib.php");
require_once("$CFG->dirroot/mod/video/lib.php");

/**
 * Redirected to migrated video if needed,
 * return if incorrect parameters specified
 * @param int $oldid
 * @param int $cmid
 * @return void
 */
function video_redirect_if_migrated($oldid, $cmid) {
    global $DB, $CFG;

    if ($oldid) {
        $old = $DB->get_record('video_old', array('oldid'=>$oldid));
    } else {
        $old = $DB->get_record('video_old', array('cmid'=>$cmid));
    }

    if (!$old) {
        return;
    }

    redirect("$CFG->wwwroot/mod/$old->newmodule/view.php?id=".$old->cmid);
}

/**
 * Display embedded video file.
 * @param object $video
 * @param object $cm
 * @param object $course
 * @param stored_file $file main file
 * @return does not return
 */
function video_display_embed($video, $cm, $course, $file) {
    global $CFG, $PAGE, $OUTPUT;

    $clicktoopen = video_get_clicktoopen($file, $video->revision);

    $context = context_module::instance($cm->id);
    $path = '/'.$context->id.'/mod_video/content/'.$video->revision.$file->get_filepath().$file->get_filename();
    $fullurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, false);
    $moodleurl = new moodle_url('/pluginfile.php' . $path);

    $mimetype = $file->get_mimetype();
    $title    = $video->name;

    $extension = videolib_get_extension($file->get_filename());

    $mediamanager = core_media_manager::instance($PAGE);
    $embedoptions = array(
        core_media_manager::OPTION_TRUSTED => true,
        core_media_manager::OPTION_BLOCK => true,
    );

    // @TODO: embed video code here

    if (file_mimetype_in_typegroup($mimetype, 'web_image')) {  // It's an image
        $code = videolib_embed_image($fullurl, $title);

    } else if ($mimetype === 'application/pdf') {
        // PDF document
        $code = videolib_embed_pdf($fullurl, $title, $clicktoopen);

    } else if ($mediamanager->can_embed_url($moodleurl, $embedoptions)) {
        // Media (audio/video) file.
        $code = $mediamanager->embed_url($moodleurl, $title, 0, 0, $embedoptions);

    } else {
        // We need a way to discover if we are loading remote docs inside an iframe.
        $moodleurl->param('embed', 1);

        // anything else - just try object tag enlarged as much as possible
        $code = videolib_embed_general($moodleurl, $title, $clicktoopen, $mimetype);
    }

    video_print_header($video, $cm, $course);
    video_print_heading($video, $cm, $course);

    echo $code;

    video_print_intro($video, $cm, $course);

    echo $OUTPUT->footer();
    die;
}

/**
 * Display video frames.
 * @param object $video
 * @param object $cm
 * @param object $course
 * @param stored_file $file main file
 * @return does not return
 */
function video_display_frame($video, $cm, $course, $file) {
    global $PAGE, $OUTPUT, $CFG;

    $frame = optional_param('frameset', 'main', PARAM_ALPHA);

    if ($frame === 'top') {
        $PAGE->set_pagelayout('frametop');
        video_print_header($video, $cm, $course);
        video_print_heading($video, $cm, $course);
        video_print_intro($video, $cm, $course);
        echo $OUTPUT->footer();
        die;

    } else {
        $config = get_config('video');
        $context = context_module::instance($cm->id);
        $path = '/'.$context->id.'/mod_video/content/'.$video->revision.$file->get_filepath().$file->get_filename();
        $fileurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, false);
        $navurl = "$CFG->wwwroot/mod/video/view.php?id=$cm->id&amp;frameset=top";
        $title = strip_tags(format_string($course->shortname.': '.$video->name));
        $framesize = $config->framesize;
        $contentframetitle = s(format_string($video->name));
        $modulename = s(get_string('modulename','video'));
        $dir = get_string('thisdirection', 'langconfig');

        $file = <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html dir="$dir">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>$title</title>
  </head>
  <frameset rows="$framesize,*">
    <frame src="$navurl" title="$modulename" />
    <frame src="$fileurl" title="$contentframetitle" />
  </frameset>
</html>
EOF;

        @header('Content-Type: text/html; charset=utf-8');
        echo $file;
        die;
    }
}

/**
 * Internal function - create click to open text with link.
 */
function video_get_clicktoopen($file, $revision, $extra='') {
    global $CFG;
//    print_r($file); die();

    $filename = $file->get_filename();
    $path = '/'.$file->get_contextid().'/mod_video/content/'.$revision.$file->get_filepath().$file->get_filename();
    $fullurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, false);

//    $string = get_string('clicktoopen2', 'video', "<a href=\"$fullurl\" $extra>$filename</a>");
    $code = '
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
            <link rel="stylesheet" type="text/css" href="css/style.css">
            <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
            <script src="js/script.js" type="text/javascript"></script>
            <video id="uploaded-video">
                <source src="'.$fullurl.'" type="video/webm">
                Your browser does not support HTML5 video. Click <a href="'.$fullurl.'">here to download</a>.
            </video>
            <input type="hidden" id="quiz-url" value="'.$CFG->wwwroot."/mod/quiz/view.php?id=".'" />
            <div class="control-container">
                <i class="play fa fa-play-circle"></i>
                <i class="pause fa fa-pause-circle"></i>
                <i class="stop fa fa-stop-circle"></i>
                <div class="duration-wrapper">
                    <span class="current">--</span>/<span class="duration">--</span>
                </div>
            </div>
            ';

//    return $string;
    return $code;
}

/**
 * Internal function - create click to open text with link.
 */
function video_get_clicktodownload($file, $revision) {
    global $CFG;

    $filename = $file->get_filename();
    $path = '/'.$file->get_contextid().'/mod_video/content/'.$revision.$file->get_filepath().$file->get_filename();
    $fullurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, true);

    $string = get_string('clicktodownload', 'video', "<a href=\"$fullurl\">$filename</a>");

    return $string;
}

/**
 * Print video info and workaround link when JS not available.
 * @param object $video
 * @param object $cm
 * @param object $course
 * @param stored_file $file main file
 * @return does not return
 */
function video_print_workaround($video, $cm, $course, $file) {
    global $CFG, $OUTPUT;

    video_print_header($video, $cm, $course);
    video_print_heading($video, $cm, $course, true);
    video_print_intro($video, $cm, $course, true);

    $video->mainfile = $file->get_filename();
    $metadata = $video->metadata?$video->metadata:'{"ids":[], "times":[], "names":[], "types":[], "urls":[]}';
    $activities = json_decode($metadata);
    ;
    if (!empty($activities->ids)) {
        echo '<div class="in-video-activities"><p><strong>In-video activities:</strong><br/><ul>';
        for ($i=0; $i<sizeof($activities->ids);$i++){
            $url = $activities->types[$i]=='quiz'?$CFG->wwwroot.'/mod/quiz/view.php?id='.$activities->ids[$i]:$activities->urls[$i];
            echo '<li><a href="'.$url.'" target="popup" onclick="window.open(\''.$url.'\',\'popup\',\'width=500,height=600\'); return false;">'.$activities->names[$i].'</a></li>';
        }
        echo '</ul></p>';
    }

    echo '<div class="videoworkaround">';
    echo '<textarea style="display: none" class="metadata">'.$metadata.'</textarea>';
    switch (video_get_final_display_type($video)) {
        case videoLIB_DISPLAY_POPUP:
            $path = '/'.$file->get_contextid().'/mod_video/content/'.$video->revision.$file->get_filepath().$file->get_filename();
            $fullurl = file_encode_url($CFG->wwwroot.'/pluginfile.php', $path, false);
            $options = empty($video->displayoptions) ? array() : unserialize($video->displayoptions);
            $width  = empty($options['popupwidth'])  ? 620 : $options['popupwidth'];
            $height = empty($options['popupheight']) ? 450 : $options['popupheight'];
            $wh = "width=$width,height=$height,toolbar=no,location=no,menubar=no,copyhistory=no,status=no,directories=no,scrollbars=yes,resizable=yes";
            $extra = "onclick=\"window.open('$fullurl', '', '$wh'); return false;\"";
            echo video_get_clicktoopen($file, $video->revision, $extra);
            break;

        case videoLIB_DISPLAY_NEW:
            $extra = 'onclick="this.target=\'_blank\'"';
            echo video_get_clicktoopen($file, $video->revision, $extra);
            break;

        case videoLIB_DISPLAY_DOWNLOAD:
            echo video_get_clicktodownload($file, $video->revision);
            break;

        case videoLIB_DISPLAY_OPEN:
        default:
            echo video_get_clicktoopen($file, $video->revision);
            break;
    }
    echo '</div>';

    echo $OUTPUT->footer();
    die;
}

/**
 * Print video header.
 * @param object $video
 * @param object $cm
 * @param object $course
 * @return void
 */
function video_print_header($video, $cm, $course) {
    global $PAGE, $OUTPUT;

    $PAGE->set_title($course->shortname.': '.$video->name);
    $PAGE->set_heading($course->fullname);
    $PAGE->set_activity_record($video);
    echo $OUTPUT->header();
}

/**
 * Print video heading.
 * @param object $video
 * @param object $cm
 * @param object $course
 * @param bool $notused This variable is no longer used
 * @return void
 */
function video_print_heading($video, $cm, $course, $notused = false) {
    global $OUTPUT;
    echo $OUTPUT->heading(format_string($video->name), 2);
}


/**
 * Gets details of the file to cache in course cache to be displayed using {@link video_get_optional_details()}
 *
 * @param object $video video table row (only property 'displayoptions' is used here)
 * @param object $cm Course-module table row
 * @return string Size and type or empty string if show options are not enabled
 */
function video_get_file_details($video, $cm) {
    $options = empty($video->displayoptions) ? array() : @unserialize($video->displayoptions);
    $filedetails = array();
    if (!empty($options['showsize']) || !empty($options['showtype']) || !empty($options['showdate'])) {
        $context = context_module::instance($cm->id);
        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'mod_video', 'content', 0, 'sortorder DESC, id ASC', false);
        // For a typical file video, the sortorder is 1 for the main file
        // and 0 for all other files. This sort approach is used just in case
        // there are situations where the file has a different sort order.
        $mainfile = $files ? reset($files) : null;
        if (!empty($options['showsize'])) {
            $filedetails['size'] = 0;
            foreach ($files as $file) {
                // This will also synchronize the file size for external files if needed.
                $filedetails['size'] += $file->get_filesize();
                if ($file->get_repository_id()) {
                    // If file is a reference the 'size' attribute can not be cached.
                    $filedetails['isref'] = true;
                }
            }
        }
        if (!empty($options['showtype'])) {
            if ($mainfile) {
                $filedetails['type'] = get_mimetype_description($mainfile);
                // Only show type if it is not unknown.
                if ($filedetails['type'] === get_mimetype_description('document/unknown')) {
                    $filedetails['type'] = '';
                }
            } else {
                $filedetails['type'] = '';
            }
        }
        if (!empty($options['showdate'])) {
            if ($mainfile) {
                // Modified date may be up to several minutes later than uploaded date just because
                // teacher did not submit the form promptly. Give teacher up to 5 minutes to do it.
                if ($mainfile->get_timemodified() > $mainfile->get_timecreated() + 5 * MINSECS) {
                    $filedetails['modifieddate'] = $mainfile->get_timemodified();
                } else {
                    $filedetails['uploadeddate'] = $mainfile->get_timecreated();
                }
                if ($mainfile->get_repository_id()) {
                    // If main file is a reference the 'date' attribute can not be cached.
                    $filedetails['isref'] = true;
                }
            } else {
                $filedetails['uploadeddate'] = '';
            }
        }
    }
    return $filedetails;
}

/**
 * Gets optional details for a video, depending on video settings.
 *
 * Result may include the file size and type if those settings are chosen,
 * or blank if none.
 *
 * @param object $video video table row (only property 'displayoptions' is used here)
 * @param object $cm Course-module table row
 * @return string Size and type or empty string if show options are not enabled
 */
function video_get_optional_details($video, $cm) {
    global $DB;

    $details = '';

    $options = empty($video->displayoptions) ? array() : @unserialize($video->displayoptions);
    if (!empty($options['showsize']) || !empty($options['showtype']) || !empty($options['showdate'])) {
        if (!array_key_exists('filedetails', $options)) {
            $filedetails = video_get_file_details($video, $cm);
        } else {
            $filedetails = $options['filedetails'];
        }
        $size = '';
        $type = '';
        $date = '';
        $langstring = '';
        $infodisplayed = 0;
        if (!empty($options['showsize'])) {
            if (!empty($filedetails['size'])) {
                $size = display_size($filedetails['size']);
                $langstring .= 'size';
                $infodisplayed += 1;
            }
        }
        if (!empty($options['showtype'])) {
            if (!empty($filedetails['type'])) {
                $type = $filedetails['type'];
                $langstring .= 'type';
                $infodisplayed += 1;
            }
        }
        if (!empty($options['showdate']) && (!empty($filedetails['modifieddate']) || !empty($filedetails['uploadeddate']))) {
            if (!empty($filedetails['modifieddate'])) {
                $date = get_string('modifieddate', 'mod_video', userdate($filedetails['modifieddate'],
                    get_string('strftimedatetimeshort', 'langconfig')));
            } else if (!empty($filedetails['uploadeddate'])) {
                $date = get_string('uploadeddate', 'mod_video', userdate($filedetails['uploadeddate'],
                    get_string('strftimedatetimeshort', 'langconfig')));
            }
            $langstring .= 'date';
            $infodisplayed += 1;
        }

        if ($infodisplayed > 1) {
            $details = get_string("videodetails_{$langstring}", 'video',
                    (object)array('size' => $size, 'type' => $type, 'date' => $date));
        } else {
            // Only one of size, type and date is set, so just append.
            $details = $size . $type . $date;
        }
    }

    return $details;
}

/**
 * Print video introduction.
 * @param object $video
 * @param object $cm
 * @param object $course
 * @param bool $ignoresettings print even if not specified in modedit
 * @return void
 */
function video_print_intro($video, $cm, $course, $ignoresettings=false) {
    global $OUTPUT;

    $options = empty($video->displayoptions) ? array() : unserialize($video->displayoptions);

    $extraintro = video_get_optional_details($video, $cm);
    if ($extraintro) {
        // Put a paragaph tag around the details
        $extraintro = html_writer::tag('p', $extraintro, array('class' => 'videodetails'));
    }

    if ($ignoresettings || !empty($options['printintro']) || $extraintro) {
        $gotintro = trim(strip_tags($video->intro));
        if ($gotintro || $extraintro) {
            echo $OUTPUT->box_start('mod_introbox', 'videointro');
            if ($gotintro) {
                echo format_module_intro('video', $video, $cm->id);
            }
            echo $extraintro;
            echo $OUTPUT->box_end();
        }
    }
}

/**
 * Print warning that instance not migrated yet.
 * @param object $video
 * @param object $cm
 * @param object $course
 * @return void, does not return
 */
function video_print_tobemigrated($video, $cm, $course) {
    global $DB, $OUTPUT;

    $video_old = $DB->get_record('video_old', array('oldid'=>$video->id));
    video_print_header($video, $cm, $course);
    video_print_heading($video, $cm, $course);
    video_print_intro($video, $cm, $course);
    echo $OUTPUT->notification(get_string('notmigrated', 'video', $video_old->type));
    echo $OUTPUT->footer();
    die;
}

/**
 * Print warning that file can not be found.
 * @param object $video
 * @param object $cm
 * @param object $course
 * @return void, does not return
 */
function video_print_filenotfound($video, $cm, $course) {
    global $DB, $OUTPUT;

    $video_old = $DB->get_record('video_old', array('oldid'=>$video->id));
    video_print_header($video, $cm, $course);
    video_print_heading($video, $cm, $course);
    video_print_intro($video, $cm, $course);
    if ($video_old) {
        echo $OUTPUT->notification(get_string('notmigrated', 'video', $video_old->type));
    } else {
        echo $OUTPUT->notification(get_string('filenotfound', 'video'));
    }
    echo $OUTPUT->footer();
    die;
}

/**
 * Decide the best display format.
 * @param object $video
 * @return int display type constant
 */
function video_get_final_display_type($video) {
    global $CFG, $PAGE;

    if ($video->display != videoLIB_DISPLAY_AUTO) {
        return $video->display;
    }

    if (empty($video->mainfile)) {
        return videoLIB_DISPLAY_DOWNLOAD;
    } else {
        $mimetype = mimeinfo('type', $video->mainfile);
    }

    if (file_mimetype_in_typegroup($mimetype, 'archive')) {
        return videoLIB_DISPLAY_DOWNLOAD;
    }
    if (file_mimetype_in_typegroup($mimetype, array('web_image', '.htm', 'web_video', 'web_audio'))) {
        return videoLIB_DISPLAY_EMBED;
    }

    // let the browser deal with it somehow
    return videoLIB_DISPLAY_OPEN;
}

/**
 * File browsing support class
 */
class video_content_file_info extends file_info_stored {
    public function get_parent() {
        if ($this->lf->get_filepath() === '/' and $this->lf->get_filename() === '.') {
            return $this->browser->get_file_info($this->context);
        }
        return parent::get_parent();
    }
    public function get_visible_name() {
        if ($this->lf->get_filepath() === '/' and $this->lf->get_filename() === '.') {
            return $this->topvisiblename;
        }
        return parent::get_visible_name();
    }
}

function video_set_mainfile($data) {
    global $DB;
    $fs = get_file_storage();
    $cmid = $data->coursemodule;
    $draftitemid = $data->files;

    $context = context_module::instance($cmid);
    if ($draftitemid) {
        $options = array('subdirs' => true, 'embed' => false);
        if ($data->display == videoLIB_DISPLAY_EMBED) {
            $options['embed'] = true;
        }
        file_save_draft_area_files($draftitemid, $context->id, 'mod_video', 'content', 0, $options);
    }
    $files = $fs->get_area_files($context->id, 'mod_video', 'content', 0, 'sortorder', false);
    if (count($files) == 1) {
        // only one file attached, set it as main file automatically
        $file = reset($files);
        file_set_sortorder($context->id, 'mod_video', 'content', 0, $file->get_filepath(), $file->get_filename(), 1);
    }
}
