<?php

/**
 * Video module upgrade code
 *
 * This file keeps track of upgrades to
 * the video module
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

function xmldb_video_upgrade($oldversion) {
    // nothing to do, because it's my 1st version ;-)
    return true;
}
