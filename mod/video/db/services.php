<?php
/**
 * Video external functions and service definitions.
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$functions = array(

    'mod_video_view_video' => array(
        'classname'     => 'mod_video_external',
        'methodname'    => 'view_video',
        'description'   => 'Simulate the view.php web interface video: trigger events, completion, etc...',
        'type'          => 'write',
        'capabilities'  => 'mod/video:view',
        'services'      => array(MOODLE_OFFICIAL_MOBILE_SERVICE)
    ),
    'mod_video_get_videos_by_courses' => array(
        'classname'     => 'mod_video_external',
        'methodname'    => 'get_videos_by_courses',
        'description'   => 'Returns a list of videos in a provided list of courses, if no list is provided all videos that
                            the user can view will be returned.',
        'type'          => 'read',
        'capabilities'  => 'mod/video:view',
        'services'      => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
    ),
);
