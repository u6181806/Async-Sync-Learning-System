<?php

/**
 * Definition of log events
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$logs = array(
    array('module'=>'video', 'action'=>'view', 'mtable'=>'video', 'field'=>'name'),
    array('module'=>'video', 'action'=>'view all', 'mtable'=>'video', 'field'=>'name'),
    array('module'=>'video', 'action'=>'update', 'mtable'=>'video', 'field'=>'name'),
    array('module'=>'video', 'action'=>'add', 'mtable'=>'video', 'field'=>'name'),
);