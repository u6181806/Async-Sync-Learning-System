<?php
/**
 * Video module version information
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2018081500;       // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2018050800;    // Requires this Moodle version
$plugin->component = 'mod_video'; // Full name of the plugin (used for diagnostics)
$plugin->cron      = 0;
