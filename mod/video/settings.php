<?php

/**
 * Video module admin settings and defaults
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    require_once("$CFG->libdir/resourcelib.php");

    $displayoptions = resourcelib_get_displayoptions(array(RESOURCELIB_DISPLAY_AUTO,
                                                           RESOURCELIB_DISPLAY_EMBED,
                                                           RESOURCELIB_DISPLAY_FRAME,
                                                           RESOURCELIB_DISPLAY_DOWNLOAD,
                                                           RESOURCELIB_DISPLAY_OPEN,
                                                           RESOURCELIB_DISPLAY_NEW,
                                                           RESOURCELIB_DISPLAY_POPUP,
                                                          ));
    $defaultdisplayoptions = array(RESOURCELIB_DISPLAY_AUTO,
                                   RESOURCELIB_DISPLAY_EMBED,
                                   RESOURCELIB_DISPLAY_DOWNLOAD,
                                   RESOURCELIB_DISPLAY_OPEN,
                                   RESOURCELIB_DISPLAY_POPUP,
                                  );

    //--- general settings -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_configtext('video/framesize',
        get_string('framesize', 'video'), get_string('configframesize', 'video'), 130, PARAM_INT));
    $settings->add(new admin_setting_configmultiselect('video/displayoptions',
        get_string('displayoptions', 'video'), get_string('configdisplayoptions', 'video'),
        $defaultdisplayoptions, $displayoptions));

    //--- modedit defaults -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_heading('videomodeditdefaults', get_string('modeditdefaults', 'admin'), get_string('condifmodeditdefaults', 'admin')));

    $settings->add(new admin_setting_configcheckbox('video/printintro',
        get_string('printintro', 'video'), get_string('printintroexplain', 'video'), 1));
    $settings->add(new admin_setting_configselect('video/display',
        get_string('displayselect', 'video'), get_string('displayselectexplain', 'video'), RESOURCELIB_DISPLAY_AUTO,
        $displayoptions));
    $settings->add(new admin_setting_configcheckbox('video/showsize',
        get_string('showsize', 'video'), get_string('showsize_desc', 'video'), 0));
    $settings->add(new admin_setting_configcheckbox('video/showtype',
        get_string('showtype', 'video'), get_string('showtype_desc', 'video'), 0));
    $settings->add(new admin_setting_configcheckbox('video/showdate',
        get_string('showdate', 'video'), get_string('showdate_desc', 'video'), 0));
    $settings->add(new admin_setting_configtext('video/popupwidth',
        get_string('popupwidth', 'video'), get_string('popupwidthexplain', 'video'), 620, PARAM_INT, 7));
    $settings->add(new admin_setting_configtext('video/popupheight',
        get_string('popupheight', 'video'), get_string('popupheightexplain', 'video'), 450, PARAM_INT, 7));
    $options = array('0' => get_string('none'), '1' => get_string('allfiles'), '2' => get_string('htmlfilesonly'));
    $settings->add(new admin_setting_configselect('video/filterfiles',
        get_string('filterfiles', 'video'), get_string('filterfilesexplain', 'video'), 0, $options));
}
