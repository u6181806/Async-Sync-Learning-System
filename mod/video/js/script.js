var inVideoActivities,
    videoDuration=0,
    video,
    lastSec = -1; // last updated playback time
$(document).ready(function(){
    inVideoActivities = $(".videoworkaround .metadata").val();
    if (inVideoActivities) {
        inVideoActivities = JSON.parse(inVideoActivities);
    }
    setTimeout(function(){
        video = document.querySelector('video[id="uploaded-video"]');
        if (video) {
            videoDuration = Math.floor(video.duration);
        }
        $(".control-container .duration-wrapper .duration").text(convertSecondsToTimeString(videoDuration));
    }, 2000);

    $(".control-container .play").click(function(){
        $(this).hide();
        $(".control-container .pause").show();
        video.play();
    });
    $(".control-container .pause").click(function(){
        $(this).hide();
        $(".control-container .play").show();
        video.pause();
    });
    $(".control-container .stop").click(function(){
        video.pause();
        video.currentTime = 0;
    });

    $('video[id="uploaded-video"]').on("timeupdate", function(){
        let sec = Math.floor(this.currentTime);
        if (sec == lastSec) return;
        lastSec = sec;
        $(".control-container .duration-wrapper .current").text(convertSecondsToTimeString(sec));
        if (inVideoActivities.times.includes(sec.toString()) || inVideoActivities.times.includes(sec)) {
            $(".control-container .pause").click();
            let a = getActivity(sec),
                msg =a.type=='quiz'?"It's time for "+a.name+". Do you want to attempt this now?":"It's time for "+a.name+". Do you want to join?"
            if (confirm(msg)) {
                let url = a.type=='quiz'?$("#quiz-url").val()+a.id:a.url;
                window.open(url, a.name,'height=500,width=600');
            }
        }
    });

});

function getActivity(sec) {
    let q = {name:null, id:null, type: null, url:null};
    for (let i=0; i<inVideoActivities.times.length; i++) {
        if (inVideoActivities.times[i] == sec) {
            q.id = inVideoActivities.ids[i];
            q.name = inVideoActivities.names[i];
            q.type = inVideoActivities.types[i];
            q.url = inVideoActivities.urls[i];
            return q;
        }
    }
}

function convertSecondsToTimeString(seconds) {
    let hour = Math.floor(seconds/3600),
        min = Math.floor((seconds - hour*3600)/60),
        sec = seconds - hour*3600 - min*60;
    let h, m, s;
    if (hour > 0 && hour<10) {
        h = "0"+hour+":";
    }
    else {
        if (hour >=10) {
            h = hour+":";
        }
        else
            h = "";
    }
    m = min<10?"0"+min+":":min+":";
    s = sec<10?"0"+sec:sec;
    return h+m+s;
}