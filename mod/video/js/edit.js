$(document).ready(function () {
    updateActivities();
    $('input[type=radio][name="activity-type"]').change(function () {
        $('#id_time').val('');
        if (this.value == 'quiz') {
            $("#id_quiz").show();
            $("#id_forum-topic").hide();
            $("#id_forum-url").hide();
        }
        else {
            $("#id_quiz").hide();
            $("#id_forum-url").show();
            $("#id_forum-topic").show();
        }
    });
    $('#id_addact').click(function () {
        let actText = $("#id_metadata").val()?$("#id_metadata").val():'{}',
            activities = JSON.parse(actText),
            actType = $("#id_activity-type_quiz").prop('checked')?'quiz':'forum',
            actTime = parseInt($('#id_time').val()),
            actName = actType=='quiz'?$('#id_quiz option:selected').text():$("#id_forum-topic").val(),
            forumUrl = $("#id_forum-url").val(),
            actId;
        if (actType=='forum' && !actName){
            alert('Invalid discussion topic.');
            $("#id_forum-topic").focus();
            return false;
        }
        if (actType=='forum' && !forumUrl){
            alert('Invalid discussion URL.');
            $("#id_forum-url").focus();
            return false;
        }
        if (!actTime || actTime<= 0){
            alert('Invalid activity time.');
            $("#id_time").focus();
            return false;
        }
        if (actType=='forum') {
            actId = 0;
        }
        else {
            actId = $('#id_quiz').val();
        }
        if (!activities.ids) {
            activities.ids = [];
        }
        if (!activities.times) {
            activities.times = [];
        }
        if (!activities.names) {
            activities.names = [];
        }
        if (!activities.types) {
            activities.types = [];
        }
        if (!activities.urls) {
            activities.urls = [];
        }
        activities.ids.push(actId);
        activities.times.push(actTime);
        activities.types.push(actType);
        activities.names.push(actName);
        activities.urls.push(actType=='quiz'?'-':forumUrl);
        $("#id_metadata").val(JSON.stringify(activities));
        updateActivities();
        $('#id_time').val('');
        $('#id_forum-topic').val('');
        $('#id_forum-url').val('');
    });
    $('.activity-container').on('click', '.actions .edit', function(){
        let actType  = $(this).parent().prev().text();
        $(this).parent().parent().find('.time input').removeAttr('disabled');
        if (actType=='forum') {
            $(this).parent().parent().find('.name input').removeAttr('disabled');
            $(this).parent().parent().find('.url input').removeAttr('disabled');
        }
    });
    $('.activity-container').on('click', '.actions .remove', function(){
        if (confirm('Are you sure to remove this activity?')){
            $(this).parent().parent().remove();
            saveChanges();
            updateActivities();
        }
    });
    $('.activity-container').on('keyup', '.name input', function(){
        saveChanges();
    });
    $('.activity-container').on('keyup', '.time input', function(){
        saveChanges();
    });
    $('.activity-container').on('keyup', '.url input', function(){
        saveChanges();
    });
});

function saveChanges() {
    let activities={ids:[], times:[], names:[], types:[], urls:[]};
    $('.activity-container .table-content .item-wrapper').each(function(){
        activities.ids.push($(this).find('.id').val());
        activities.times.push($(this).find('.time input').val());
        activities.names.push($(this).find('.name input').val());
        activities.urls.push($(this).find('.url input').val());
        activities.types.push($(this).find('.type').text());
    });
    $("#id_metadata").val(JSON.stringify(activities));
}

function updateActivities() {
    let actText = $("#id_metadata").val()?$("#id_metadata").val():'{"ids":[], "times":[], "names":[], "types":[], "urls":[]}',
        activities = JSON.parse(actText),
        h='',
        actName,
        actUrl,
        actType,
        actTime;


    for (let i=0; i<activities.ids.length; i++){
        actName = activities.names[i];
        actType = activities.types[i];
        actTime = activities.times[i];
        actUrl = activities.urls[i];
        h += '<div class="item-wrapper"><input type="hidden" value="'+activities.ids[i]+'" class="id"/>'+
            '<div class="name"><input type="text" class="form-control" disabled value="'+actName+'"/></div>'+
            '<div class="url"><input type="text" class="form-control" disabled value="'+actUrl+'"/></div>'+
            '<div class="time"><input type="text" class="form-control" disabled value="'+actTime+'"/></div><div class="type">'+actType+'</div>'+
            '<div class="actions"><div class="edit"></div><div class="remove"></div></div></div>';
    }

    if (!activities.ids.length) {
        h ='None';
    }
    $('.activity-container .table-content').html(h);
}