<?php
/**
 * Search area for mod_video activities.
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_video\search;

defined('MOODLE_INTERNAL') || die();

/**
 * Search area for mod_video activities.
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class activity extends \core_search\base_activity {

    /**
     * Returns true if this area uses file indexing.
     *
     * @return bool
     */
    public function uses_file_indexing() {
        return true;
    }

    /**
     * Return the context info required to index files for
     * this search area.
     *
     * @return array
     */
    public function get_search_fileareas() {
        $fileareas = array('intro', 'content'); // Fileareas.

        return $fileareas;
    }
}
