<?php

/**
 * Video module version information
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/video/lib.php');
require_once($CFG->dirroot.'/mod/video/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

$id       = optional_param('id', 0, PARAM_INT); // Course Module ID
$r        = optional_param('r', 0, PARAM_INT);  // Video instance ID
$redirect = optional_param('redirect', 0, PARAM_BOOL);
$forceview = optional_param('forceview', 0, PARAM_BOOL);

if ($r) {
    if (!$video = $DB->get_record('video', array('id'=>$r))) {
        video_redirect_if_migrated($r, 0);
        print_error('invalidaccessparameter');
    }
    $cm = get_coursemodule_from_instance('video', $video->id, $video->course, false, MUST_EXIST);

} else {
    if (!$cm = get_coursemodule_from_id('video', $id)) {
        video_redirect_if_migrated(0, $id);
        print_error('invalidcoursemodule');
    }
    $video = $DB->get_record('video', array('id'=>$cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/video:view', $context);

// Completion and trigger events.
video_view($video, $course, $cm, $context);

$PAGE->set_url('/mod/video/view.php', array('id' => $cm->id));

if ($video->tobemigrated) {
    video_print_tobemigrated($video, $cm, $course);
    die;
}

$fs = get_file_storage();
$files = $fs->get_area_files($context->id, 'mod_video', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
if (count($files) < 1) {
    video_print_filenotfound($video, $cm, $course);
    die;
} else {
    $file = reset($files);
    unset($files);
}

$video->mainfile = $file->get_filename();
$displaytype = video_get_final_display_type($video);
if ($displaytype == VIDEOLIB_DISPLAY_OPEN || $displaytype == VIDEOLIB_DISPLAY_DOWNLOAD) {
    $redirect = true;
}

// Don't redirect teachers, otherwise they can not access course or module settings.
if ($redirect && !course_get_format($course)->has_view_page() &&
        (has_capability('moodle/course:manageactivities', $context) ||
        has_capability('moodle/course:update', context_course::instance($course->id)))) {
    $redirect = false;
}

if ($redirect && !$forceview) {
    // coming from course page or url index page
    // this redirect trick solves caching problems when tracking views ;-)
    $path = '/'.$context->id.'/mod_video/content/'.$video->revision.$file->get_filepath().$file->get_filename();
    $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, $displaytype == VIDEOLIB_DISPLAY_DOWNLOAD);
    redirect($fullurl);
}

switch ($displaytype) {
    case VIDEOLIB_DISPLAY_EMBED:
        video_display_embed($video, $cm, $course, $file);
        break;
    case VIDEOLIB_DISPLAY_FRAME:
        video_display_frame($video, $cm, $course, $file);
        break;
    default:
        video_print_workaround($video, $cm, $course, $file);
        break;
}

