<?php
/**
 * Strings for component 'video', language 'en'
 *
 * @package    mod_video
 * @copyright  2018 Sam Ngo  {@link mailto:giangnt1305@gmail.com}
 *             ANU Master of Computing student - u6181806
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['clicktodownload'] = 'Click {$a} link to download the file.';
$string['clicktoopen2'] = 'Click {$a} link to view the file.';
$string['configdisplayoptions'] = 'Select all options that should be available, existing settings are not modified. Hold CTRL key to select multiple fields.';
$string['configframesize'] = 'When a web page or an uploaded file is displayed within a frame, this value is the height (in pixels) of the top frame (which contains the navigation).';
$string['configparametersettings'] = 'This sets the default value for the Parameter settings pane in the form when adding some new videos. After the first time, this becomes an individual user preference.';
$string['configpopup'] = 'When adding a new video which is able to be shown in a popup window, should this option be enabled by default?';
$string['configpopupdirectories'] = 'Should popup windows show directory links by default?';
$string['configpopupheight'] = 'What height should be the default height for new popup windows?';
$string['configpopuplocation'] = 'Should popup windows show the location bar by default?';
$string['configpopupmenubar'] = 'Should popup windows show the menu bar by default?';
$string['configpopupresizable'] = 'Should popup windows be resizable by default?';
$string['configpopupscrollbars'] = 'Should popup windows be scrollable by default?';
$string['configpopupstatus'] = 'Should popup windows show the status bar by default?';
$string['configpopuptoolbar'] = 'Should popup windows show the tool bar by default?';
$string['configpopupwidth'] = 'What width should be the default width for new popup windows?';
$string['contentheader'] = 'Content';
$string['displayoptions'] = 'Available display options';
$string['displayselect'] = 'Display';
$string['displayselect_help'] = 'This setting, together with the file type and whether the browser allows embedding, determines how the file is displayed. Options may include:

* Automatic - The best display option for the file type is selected automatically
* Embed - The file is displayed within the page below the navigation bar together with the file description and any blocks
* Force download - The user is prompted to download the file
* Open - Only the file is displayed in the browser window
* In pop-up - The file is displayed in a new browser window without menus or an address bar
* In frame - The file is displayed within a frame below the navigation bar and file description
* New window - The file is displayed in a new browser window with menus and an address bar';
$string['displayselect_link'] = 'mod/file/mod';
$string['displayselectexplain'] = 'Choose display type, unfortunately not all types are suitable for all files.';
$string['dnduploadvideo'] = 'Create file video';
$string['encryptedcode'] = 'Encrypted code';
$string['filenotfound'] = 'File not found, sorry.';
$string['filterfiles'] = 'Use filters on file content';
$string['filterfilesexplain'] = 'Select type of file content filtering, please note this may cause problems for some Flash and Java applets. Please make sure that all text files are in UTF-8 encoding.';
$string['filtername'] = 'video names auto-linking';
$string['forcedownload'] = 'Force download';
$string['framesize'] = 'Frame height';
$string['indicator:cognitivedepth'] = 'File cognitive';
$string['indicator:cognitivedepth_help'] = 'This indicator is based on the cognitive depth reached by the student in a File video.';
$string['indicator:socialbreadth'] = 'File social';
$string['indicator:socialbreadth_help'] = 'This indicator is based on the social breadth reached by the student in a File video.';
$string['legacyfiles'] = 'Migration of old course file';
$string['legacyfilesactive'] = 'Active';
$string['legacyfilesdone'] = 'Finished';
$string['modifieddate'] = 'Modified {$a}';
$string['modulename'] = 'In-video activity';
$string['modulename_help'] = 'The In-video activity enables a teacher to provide a file as a course video. Where possible, the video will be displayed within the course interface; otherwise students will be prompted to download it. 

Supported video format:

- WebM

- Ogg Theora Vorbis

- Ogg Opus

- Ogg FLAC

- MP4 H.264 (AAC or MP3)

- MP4 FLAC

- MP3

- WAVE PCM

- FLAC

This activity can also embed quizzes and forum discussions into the video. The teacher should specify the time and activity (e.g. quiz or discussion topic) in order to show them when video is played.';
$string['modulename_link'] = 'mod/video/view';
$string['modulenameplural'] = 'Videos';
$string['notmigrated'] = 'This legacy video type ({$a}) was not yet migrated, sorry.';
$string['optionsheader'] = 'Display options';
$string['page-mod-video-x'] = 'Any file module page';
$string['pluginadministration'] = 'File module administration';
$string['pluginname'] = 'In-video activity';
$string['popupheight'] = 'Pop-up height (in pixels)';
$string['popupheightexplain'] = 'Specifies default height of popup windows.';
$string['popupvideo'] = 'This video should appear in a popup window.';
$string['popupvideolink'] = 'If it didn\'t, click here: {$a}';
$string['popupwidth'] = 'Pop-up width (in pixels)';
$string['popupwidthexplain'] = 'Specifies default width of popup windows.';
$string['printintro'] = 'Display video description';
$string['printintroexplain'] = 'Display video description below content? Some display types may not display description even if enabled.';
$string['privacy:metadata'] = 'The File video plugin does not store any personal data.';
$string['video:addinstance'] = 'Add a new video';
$string['videocontent'] = 'Files and subfolders';
$string['videodetails_sizetype'] = '{$a->size} {$a->type}';
$string['videodetails_sizedate'] = '{$a->size} {$a->date}';
$string['videodetails_typedate'] = '{$a->type} {$a->date}';
$string['videodetails_sizetypedate'] = '{$a->size} {$a->type} {$a->date}';
$string['video:exportvideo'] = 'Export video';
$string['video:view'] = 'View video';
$string['search:activity'] = 'File';
$string['selectmainfile'] = 'Please select the main video by clicking the icon next to video name.';
$string['showdate'] = 'Show upload/modified date';
$string['showdate_desc'] = 'Display upload/modified date on course page?';
$string['showdate_help'] = 'Displays the upload/modified date beside links to the file.

If there are multiple files in this video, the start file upload/modified date is displayed.';
$string['showsize'] = 'Show size';
$string['showsize_help'] = 'Displays the file size, such as \'3.1 MB\', beside links to the file.

If there are multiple files in this video, the total size of all files is displayed.';
$string['showsize_desc'] = 'Display file size on course page?';
$string['showtype'] = 'Show type';
$string['showtype_desc'] = 'Display file type (e.g. \'Word document\') on course page?';
$string['showtype_help'] = 'Displays the type of the file, such as \'Word document\', beside links to the file.

If there are multiple files in this video, the start file type is displayed.

If the file type is not known to the system, it will not display.';
$string['uploadeddate'] = 'Uploaded {$a}';

$string['activities'] = "Activities";
$string['activityselect'] = "Activities";
$string['videoactivityquiz'] = "Quiz";
$string['addnewactivity'] = "Add activity";
$string['noactivity'] = "None";
$string['selectactivity'] = "Select an activity...";
$string['associatedactivities'] = "Associated activities";